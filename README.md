## Start ##
When making changes try not to edit the scene, make prefabs.

And also start making branches for changes

# What to add #

	1. Camera at 45 degree angles
		- movement to follow player
			- slowish catch up?
	2. Character movement (Somewhat done)
		- Collisions
                        -walls
		- Clamp bounds
		- Jumping
		- Acceleration to prevent the player from being a 0 to being a hero(speedy person) in the click on a button
	3. Character shooting
                -projectiles or instantHit
                - Projectiles could be cool if they have lights or are self illuminating 
	4. Enemy's
		- Stationary
	5. Level
		- walls could be like tron/ARMA 3 VR https://arma3.com/assets/img/post/images/roadmap_14_15_10.jpg