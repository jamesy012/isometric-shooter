﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{

	/// <summary>
	/// shooting projectile
	/// </summary>
	public GameObject projectile;

	/// <summary>
	/// max speed for player movement
	/// </summary>
	public float m_MaxSpeed = 5.0f;

	/// <summary>
	/// speed at which the animation plays in seconds
	/// </summary>
	public float m_AnimationSpeed = 0.1f;
	/// <summary>
	/// curve for speeding the player up and slowing the player down
	/// </summary>
	public AnimationCurve m_SpeedCurve;

	/// <summary>
	/// 0 - Not Moving
	/// 1 - Started Moving
	///	2 - Moving
	///	3 - Finished Moving
	///</summary>
	private int m_MovementStage = 0;
	/// <summary>
	/// used for the AnimationCurves so get the position in their time line
	/// </summary>
	private float m_AnimationSeekPos = 0;

	/// <summary>
	/// characterController for the script
	/// </summary>
	private CharacterController m_charController;

	/// <summary>
	/// y height for lookAt rotation at projectile shooting
	///</summary>
	private float m_YLookatHeight = 0.5f;

	/// <summary>
	/// storage of the last movement direction
	///</summary>
	private Vector3 m_LastMovementDirection;

	/// <summary>
	/// for shooting direction and look rotation
	///</summary>
	private Vector3 m_targetPoint = new Vector3();

	/// <summary>
	/// Use this for initialization
	///</summary>
	bool mouseLeftPressed = false;

    /// <summary>
    /// Dash flag
    /// </summary>
    private bool dashing = false;

    /// <summary>
    /// Blink Distance
    /// </summary>
    public float blinkDistance = 1.0f;

    /// <summary>
    /// Dash cooldown
    /// </summary>
    public float dashCooldown = 1.0f;


    /// <summary>
    /// TimeStamp
    /// </summary>
    float timeStamp = 0.0f;

    private ParticleSystem ps;
    

    void Start()
    {
 
        m_charController = GetComponent<CharacterController>();
		//getting y position
        m_YLookatHeight = transform.position.y;

        ps = GetComponent<ParticleSystem>();

    }

    // Update is called once per frame
    void Update()
    {


		//shoot
        if (Input.GetButtonDown("Fire1"))
        {
            mouseLeftPressed = true;
        }
        m_targetPoint.y = m_YLookatHeight;





        //movement
        movePlayer();        


        //rotation
        //Could put into it's own class and pass to the player
        Plane plane = new Plane(Vector3.up, 0);
        float dist;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (plane.Raycast(ray, out dist))
        {
            m_targetPoint = ray.GetPoint(dist) + new Vector3(0, m_YLookatHeight, 0);
            
            transform.LookAt(m_targetPoint);
        }

        if (Input.GetKeyDown(KeyCode.Space) && timeStamp <= Time.time)//trigger dash
        {
            ps.Play();
            dashing = true;
        }






    }

	/// <summary>
	/// code that will move the player based on axis information
	/// also handles speed up and slowdown states
	/// </summary>
	private void movePlayer() {
		Vector3 moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
		//to make sure pressing W and D will not create a faster speed
		moveDirection.Normalize();
	
		//state switch code
		switch (m_MovementStage) {
			case 0://NOT MOVING
				if (moveDirection != Vector3.zero) {
					m_MovementStage = 1;
					//******Add Particles (like dust?)
				}
				break;
			case 1://GAINING SPEED
				if (moveDirection == Vector3.zero) {
					m_MovementStage = 3;
					break;
				}
				if (m_AnimationSeekPos >= 1) {
					m_AnimationSeekPos = 1;
					m_MovementStage = 2;
				}
				break;
			case 2://NORMAL MOVEMENT
				if (moveDirection == Vector3.zero) {
					m_MovementStage = 3;
				}
				break;
			case 3://LOSING SPEED
				if (moveDirection != Vector3.zero) {
					m_MovementStage = 1;
					break;
				}
				if (m_AnimationSeekPos <= 0) {
					m_AnimationSeekPos = 0;
					m_MovementStage = 0;
				}
				break;
		}

		//if moving
		if (m_MovementStage != 0) {
			float maxSpeedMultiplyer = 1;
			

			if (m_MovementStage == 1) {
				m_AnimationSeekPos += Time.deltaTime;
				maxSpeedMultiplyer = m_SpeedCurve.Evaluate(m_AnimationSeekPos);
			}

			if (m_MovementStage == 3) {//m_MovementStage of 1 or 2, using the last direction made
				m_AnimationSeekPos -= Time.deltaTime;
				maxSpeedMultiplyer = m_SpeedCurve.Evaluate(m_AnimationSeekPos);
				
			}

			//either m_MovementStage of 1 or 2, using the real direction
			if (m_MovementStage==1||m_MovementStage==2) {

				moveDirection = Camera.main.transform.TransformDirection(moveDirection);
				moveDirection.y = 0;

				m_charController.Move(moveDirection * (maxSpeedMultiplyer * m_MaxSpeed) * Time.deltaTime);
				m_LastMovementDirection = moveDirection;
			}

			if(m_MovementStage == 3) {
				m_charController.Move(m_LastMovementDirection * (maxSpeedMultiplyer * m_MaxSpeed) * Time.deltaTime);
			}
		}
	}

	//Fixed Update
	void FixedUpdate()
    {
        if (mouseLeftPressed)
        {


            Vector3 projectileDir = this.transform.position - m_targetPoint;


            GameObject obj = (GameObject)Instantiate(projectile, new Vector3(0,m_YLookatHeight,0), Quaternion.identity);

            obj.GetComponent<Rigidbody>().position = this.transform.position - projectileDir.normalized;
            obj.GetComponent<ProjectileScript>().m_Direction = -projectileDir.normalized;



            mouseLeftPressed = false;//reset
        }


        if (dashing)
        {
            Vector3 dashBlinkLocation = transform.position - m_targetPoint;
            dashBlinkLocation.y = 0;
            dashBlinkLocation.Normalize();
            m_charController.Move(-dashBlinkLocation * blinkDistance * Time.deltaTime);
            timeStamp = Time.time + dashCooldown;//cooldown in seconds
            dashing = false;
            
           
        }



    }





}