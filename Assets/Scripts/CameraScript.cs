﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{


    public GameObject m_Player;

    public float Distance = 0.0f;

    private Vector3 m_Offset;

    void Start()
    {
        if (m_Player == null)
        {
            return;
        }
        m_Offset = transform.position - m_Player.transform.position;
    }


    void Update()
    {
        if (m_Player == null)//if player dies dont crash
        {
            return;
        }



        Distance = (transform.position - m_Player.transform.position).magnitude;//debugging thing


        transform.position = m_Player.transform.position + m_Offset;
        

        if (Input.GetKey(KeyCode.Q))
        {
            transform.position = RotatePointAroundPivot(transform.position, m_Player.transform.position, Vector3.up);


        }

        else
        if (Input.GetKey(KeyCode.E))
        {
            transform.position = RotatePointAroundPivot(transform.position, m_Player.transform.position, Vector3.down);

        }

        m_Offset = transform.position - m_Player.transform.position;//////THIS WAS NEEDED



        transform.LookAt(m_Player.transform.position);
    }


    //http://answers.unity3d.com/questions/532297/rotate-a-vector-around-a-certain-point.html
    public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
    {
        Vector3 dir = point - pivot; // get point direction relative to pivot
        dir = Quaternion.Euler(angles) * dir; // rotate it
        point = dir + pivot; // calculate rotated point
        return point;
    }
}






