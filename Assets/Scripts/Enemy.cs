﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy : MonoBehaviour
{

    /// <summary>
    /// projectile prefab
    /// </summary>
    public GameObject m_Projectile;
    /// <summary>
    /// transform to look at
    /// </summary>
    private Transform m_PlayerTransform;
    /// <summary>
    /// point to shoot from
    /// </summary>
    private Transform m_ShootPoint;
    /// <summary>
    /// what transform rotate 
    /// </summary>
    private Transform m_RotationTransform;

    /// <summary>
    /// max distance at which this will shoot
    /// </summary>
    public float m_Distance = 10.0f;

    /// <summary>
    /// seconds between shots
    /// </summary>
    public float m_TimeBetweenShots = 1.0f;
    /// <summary>
    /// storage of the last time this enemy shot
    /// </summary>
    private float m_LastShotTime;

    // Use this for initialization
    void Start()
    {
        m_PlayerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        m_ShootPoint = transform.GetChild(1).GetChild(2);
        m_RotationTransform = transform.GetChild(1);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_PlayerTransform == null)
        {
            return;
        }


        //if to far away, return
        if (Vector3.Distance(transform.position, m_PlayerTransform.position) > m_Distance)
        {
            return;
        }



        RaycastHit objectHit;
        // Shoot raycast
        if (Physics.Raycast(m_ShootPoint.position, m_ShootPoint.forward, out objectHit, m_Distance))
        {
            if (objectHit.transform.CompareTag("Player"))
            {
                m_RotationTransform.LookAt(m_PlayerTransform.position);
                
                shootProjectile(objectHit.point);


            }
            else
            {
                m_RotationTransform.Rotate(Vector3.up);
            }





        }

        //m_RotationTransform.Rotate(new Vector3(0, -90, 0));
    }

    /// <summary>
    /// shoots projectile in a direction
    /// </summary>
    /// <param name="a_Dir">position to shoot towards</param>
    private void shootProjectile(Vector3 a_Pos)
    {
        if (Time.time - m_TimeBetweenShots > m_LastShotTime)
        {
            

            Vector3 projectileDir = this.transform.position - a_Pos;
            projectileDir.y = 0;
            GameObject obj = (GameObject)Instantiate(m_Projectile, m_ShootPoint.position, Quaternion.identity);

            //obj.GetComponent<Rigidbody>().position = m_ShootPoint.position;
            obj.GetComponent<ProjectileScript>().m_Direction = -projectileDir.normalized;

            m_LastShotTime = Time.time;
        }
    }
}
