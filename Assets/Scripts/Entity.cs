﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {

	/// <summary>
	/// check box to set whether the health bar will be created or not
	/// </summary>
	public bool m_ShowHealthBar = true;
	/// <summary>
	/// if this is checked then the health bar will rotate
	/// </summary>
	public bool m_RotateHealthBar = true;

	/// <summary>
	/// starting health value
	/// </summary>
	public float m_StartHealth = 10.0f;
	/// <summary>
	/// current health value
	/// </summary>
	private float m_CurrentHealth;

	/// <summary>
	/// this is a link to the object that holds the health
	/// </summary>
	private GameObject m_HealthHolder;

	/// <summary>
	/// this is the red health bar, this should never change size
	/// </summary>
	private GameObject m_FullHealth;
	/// <summary>
	/// this is the green health bar, this should change to repersent current health
	/// </summary>
	private GameObject m_RemainingHealth;

	// Use this for initialization
	void Start() {
		m_CurrentHealth = m_StartHealth;

		if (m_ShowHealthBar) {
			m_HealthHolder = new GameObject();
			m_HealthHolder.transform.SetParent(transform);

			m_HealthHolder.name = "Health Holder";

			m_FullHealth = GameObject.CreatePrimitive(PrimitiveType.Cube);
			Destroy(m_FullHealth.GetComponent<BoxCollider>());
			m_FullHealth.transform.SetParent(m_HealthHolder.transform);
			m_FullHealth.GetComponent<Renderer>().material.color = Color.red;
			m_FullHealth.transform.localScale = new Vector3(0.99f, 0.39f, 0.2f);

			m_RemainingHealth = GameObject.CreatePrimitive(PrimitiveType.Cube);
			Destroy(m_RemainingHealth.GetComponent<BoxCollider>());
			m_RemainingHealth.transform.SetParent(m_HealthHolder.transform);
			m_RemainingHealth.transform.localScale = new Vector3(1f, 0.4f, 0.3f);
			m_RemainingHealth.GetComponent<Renderer>().material.color = Color.green;

			m_HealthHolder.transform.position = transform.position + new Vector3(0, 2, 0);
		}
	}
	

	void LateUpdate() {
		if (!m_RotateHealthBar) {
			return;
		}
		//look towards camera
		m_HealthHolder.transform.LookAt(Camera.main.transform);
		float oldY = m_HealthHolder.transform.rotation.eulerAngles.y - -45/2;
		//lock to be in 45 degree angles
		float newY = Mathf.FloorToInt(oldY / 45);
		newY *= 44.5f;
		//set the rotation back
		m_HealthHolder.transform.rotation = Quaternion.Euler(new Vector3(0, newY, 0));

	}

	public void damageEntity(float a_DamageAmount) {
		m_CurrentHealth -= a_DamageAmount;

		if (m_ShowHealthBar) {
			float percentageOfLifeLeft = m_CurrentHealth / m_StartHealth;
			percentageOfLifeLeft = percentageOfLifeLeft > 1 ? percentageOfLifeLeft = 1 : percentageOfLifeLeft < 0 ? 0 : percentageOfLifeLeft;
			m_RemainingHealth.transform.position += m_RemainingHealth.transform.right * ((m_RemainingHealth.transform.localScale.x - percentageOfLifeLeft) / 2);
			m_RemainingHealth.transform.localScale = new Vector3(percentageOfLifeLeft, m_RemainingHealth.transform.localScale.y, m_RemainingHealth.transform.localScale.z);
		}

		if (m_CurrentHealth <= 0) {
			//dead
			m_CurrentHealth = 0;
			Destroy(this.gameObject);
		}

	}
}
