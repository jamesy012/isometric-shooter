﻿using UnityEngine;
using System.Collections;

public class MoveTowards : MonoBehaviour
{

    public Transform m_TargetPos;
    public float m_DampeningFactor = 0.1f;
    private Vector3 velocity = Vector3.zero;
    public float smoothTime = 0.3f;

    //public GameObject m_player;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame


    void FixedUpdate()

    {
        //https://docs.unity3d.com/Manual/DirectionDistanceFromOneObjectToAnother.html
        // Gets a vector that points from the player's position to the target's.

        transform.position = Vector3.SmoothDamp(transform.position, m_TargetPos.transform.position, ref velocity, smoothTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, m_TargetPos.transform.rotation, smoothTime);

    }
}
