﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour
{
    public Vector3 m_Direction;
    public float m_ProjectileSpeed = 1;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //move projectile
        this.transform.position += m_Direction * m_ProjectileSpeed * Time.deltaTime;
    }

    void OnCollisionEnter(Collision col)
    {
		if (col.collider.CompareTag("Wall")) {
			Destroy(this.gameObject);
			return;
		}

		if (col.collider.CompareTag("Player")) {
			Destroy(this.gameObject);
			col.gameObject.GetComponent<Entity>().damageEntity(1);
			return;
		}

		if (col.collider.CompareTag("Enemy")) {
			Destroy(this.gameObject);
			col.gameObject.GetComponent<Entity>().damageEntity(1);
			return;
		}


	}
}
