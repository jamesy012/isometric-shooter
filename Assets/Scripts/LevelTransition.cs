﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class LevelTransition : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if(col.CompareTag("Player"))
        {
           
            int index = SceneManager.GetActiveScene().buildIndex;

            index +=1;
            if (index <= SceneManager.sceneCount)
            {
                ///Level names should follow this convention: "level1", "level2" ...etc
                SceneManager.LoadScene("level" + index, LoadSceneMode.Single);

            }

           
        }
    }

}
